package com.mgoulene.mmdb.service.mapper;

import com.mgoulene.mmdb.domain.*;
import com.mgoulene.mmdb.service.dto.MyMovieDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link MyMovie} and its DTO {@link MyMovieDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MyMovieMapper extends EntityMapper<MyMovieDTO, MyMovie> {}
