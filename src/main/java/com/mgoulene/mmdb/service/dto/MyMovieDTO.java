package com.mgoulene.mmdb.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mgoulene.mmdb.domain.MyMovie} entity.
 */
@ApiModel(description = "The Entity entity.\n@author A true hipster")
public class MyMovieDTO implements Serializable {

    private Long id;

    /**
     * fieldName
     */
    @Size(max = 4000)
    @ApiModelProperty(value = "fieldName")
    private String comments;

    @Min(value = 0)
    @Max(value = 5)
    private Integer vote;

    private LocalDate viewedDate;

    @NotNull
    private String tmdbId;

    @NotNull
    private Long userId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getVote() {
        return vote;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public LocalDate getViewedDate() {
        return viewedDate;
    }

    public void setViewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
    }

    public String getTmdbId() {
        return tmdbId;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyMovieDTO)) {
            return false;
        }

        MyMovieDTO myMovieDTO = (MyMovieDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, myMovieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MyMovieDTO{" +
            "id=" + getId() +
            ", comments='" + getComments() + "'" +
            ", vote=" + getVote() +
            ", viewedDate='" + getViewedDate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
