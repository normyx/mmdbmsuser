package com.mgoulene.mmdb.service;

import com.mgoulene.mmdb.domain.MyMovie;
import com.mgoulene.mmdb.repository.MyMovieRepository;
import com.mgoulene.mmdb.service.dto.MyMovieDTO;
import com.mgoulene.mmdb.service.mapper.MyMovieMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MyMovie}.
 */
@Service
@Transactional
public class MyMovieService {

    private final Logger log = LoggerFactory.getLogger(MyMovieService.class);

    private final MyMovieRepository myMovieRepository;

    private final MyMovieMapper myMovieMapper;

    public MyMovieService(MyMovieRepository myMovieRepository, MyMovieMapper myMovieMapper) {
        this.myMovieRepository = myMovieRepository;
        this.myMovieMapper = myMovieMapper;
    }

    /**
     * Save a myMovie.
     *
     * @param myMovieDTO the entity to save.
     * @return the persisted entity.
     */
    public MyMovieDTO save(MyMovieDTO myMovieDTO) {
        log.debug("Request to save MyMovie : {}", myMovieDTO);
        MyMovie myMovie = myMovieMapper.toEntity(myMovieDTO);
        myMovie = myMovieRepository.save(myMovie);
        return myMovieMapper.toDto(myMovie);
    }

    /**
     * Partially update a myMovie.
     *
     * @param myMovieDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MyMovieDTO> partialUpdate(MyMovieDTO myMovieDTO) {
        log.debug("Request to partially update MyMovie : {}", myMovieDTO);

        return myMovieRepository
            .findById(myMovieDTO.getId())
            .map(
                existingMyMovie -> {
                    myMovieMapper.partialUpdate(existingMyMovie, myMovieDTO);
                    return existingMyMovie;
                }
            )
            .map(myMovieRepository::save)
            .map(myMovieMapper::toDto);
    }

    /**
     * Get all the myMovies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MyMovieDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MyMovies");
        return myMovieRepository.findAll(pageable).map(myMovieMapper::toDto);
    }

    /**
     * Get one myMovie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MyMovieDTO> findOne(Long id) {
        log.debug("Request to get MyMovie : {}", id);
        return myMovieRepository.findById(id).map(myMovieMapper::toDto);
    }

    /**
     * Delete the myMovie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MyMovie : {}", id);
        myMovieRepository.deleteById(id);
    }
}
