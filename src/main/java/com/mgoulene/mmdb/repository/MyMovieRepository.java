package com.mgoulene.mmdb.repository;

import com.mgoulene.mmdb.domain.MyMovie;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MyMovie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MyMovieRepository extends JpaRepository<MyMovie, Long>, JpaSpecificationExecutor<MyMovie> {}
