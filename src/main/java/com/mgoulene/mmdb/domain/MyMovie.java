package com.mgoulene.mmdb.domain;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The Entity entity.\n@author A true hipster
 */
@Entity
@Table(name = "my_movie")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MyMovie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    /**
     * fieldName
     */
    @Size(max = 4000)
    @Column(name = "comments", length = 4000)
    private String comments;

    @Min(value = 0)
    @Max(value = 5)
    @Column(name = "vote")
    private Integer vote;

    @Column(name = "viewed_date")
    private LocalDate viewedDate;

    @NotNull
    @Column(name = "tmdb_id", nullable = false)
    private String tmdbId;

    @NotNull
    @Column(name = "user_id", nullable = false)
    private Long userId;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MyMovie id(Long id) {
        this.id = id;
        return this;
    }

    public String getComments() {
        return this.comments;
    }

    public MyMovie comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public Integer getVote() {
        return this.vote;
    }

    public MyMovie vote(Integer vote) {
        this.vote = vote;
        return this;
    }

    public void setVote(Integer vote) {
        this.vote = vote;
    }

    public LocalDate getViewedDate() {
        return this.viewedDate;
    }

    public MyMovie viewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
        return this;
    }

    public void setViewedDate(LocalDate viewedDate) {
        this.viewedDate = viewedDate;
    }

    public String getTmdbId() {
        return this.tmdbId;
    }

    public MyMovie tmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
        return this;
    }

    public void setTmdbId(String tmdbId) {
        this.tmdbId = tmdbId;
    }

    public Long getUserId() {
        return this.userId;
    }

    public MyMovie userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MyMovie)) {
            return false;
        }
        return id != null && id.equals(((MyMovie) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MyMovie{" +
            "id=" + getId() +
            ", comments='" + getComments() + "'" +
            ", vote=" + getVote() +
            ", viewedDate='" + getViewedDate() + "'" +
            ", tmdbId='" + getTmdbId() + "'" +
            ", userId=" + getUserId() +
            "}";
    }
}
