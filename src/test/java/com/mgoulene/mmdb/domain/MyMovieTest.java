package com.mgoulene.mmdb.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mmdb.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MyMovieTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MyMovie.class);
        MyMovie myMovie1 = new MyMovie();
        myMovie1.setId(1L);
        MyMovie myMovie2 = new MyMovie();
        myMovie2.setId(myMovie1.getId());
        assertThat(myMovie1).isEqualTo(myMovie2);
        myMovie2.setId(2L);
        assertThat(myMovie1).isNotEqualTo(myMovie2);
        myMovie1.setId(null);
        assertThat(myMovie1).isNotEqualTo(myMovie2);
    }
}
