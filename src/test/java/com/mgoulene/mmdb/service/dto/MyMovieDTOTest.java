package com.mgoulene.mmdb.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.mgoulene.mmdb.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MyMovieDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MyMovieDTO.class);
        MyMovieDTO myMovieDTO1 = new MyMovieDTO();
        myMovieDTO1.setId(1L);
        MyMovieDTO myMovieDTO2 = new MyMovieDTO();
        assertThat(myMovieDTO1).isNotEqualTo(myMovieDTO2);
        myMovieDTO2.setId(myMovieDTO1.getId());
        assertThat(myMovieDTO1).isEqualTo(myMovieDTO2);
        myMovieDTO2.setId(2L);
        assertThat(myMovieDTO1).isNotEqualTo(myMovieDTO2);
        myMovieDTO1.setId(null);
        assertThat(myMovieDTO1).isNotEqualTo(myMovieDTO2);
    }
}
