package com.mgoulene.mmdb.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mgoulene.mmdb.IntegrationTest;
import com.mgoulene.mmdb.domain.MyMovie;
import com.mgoulene.mmdb.repository.MyMovieRepository;
import com.mgoulene.mmdb.service.criteria.MyMovieCriteria;
import com.mgoulene.mmdb.service.dto.MyMovieDTO;
import com.mgoulene.mmdb.service.mapper.MyMovieMapper;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MyMovieResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MyMovieResourceIT {

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    private static final Integer DEFAULT_VOTE = 0;
    private static final Integer UPDATED_VOTE = 1;
    private static final Integer SMALLER_VOTE = 0 - 1;

    private static final LocalDate DEFAULT_VIEWED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_VIEWED_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_VIEWED_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_TMDB_ID = "AAAAAAAAAA";
    private static final String UPDATED_TMDB_ID = "BBBBBBBBBB";

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;
    private static final Long SMALLER_USER_ID = 1L - 1L;

    private static final String ENTITY_API_URL = "/api/my-movies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MyMovieRepository myMovieRepository;

    @Autowired
    private MyMovieMapper myMovieMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMyMovieMockMvc;

    private MyMovie myMovie;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MyMovie createEntity(EntityManager em) {
        MyMovie myMovie = new MyMovie()
            .comments(DEFAULT_COMMENTS)
            .vote(DEFAULT_VOTE)
            .viewedDate(DEFAULT_VIEWED_DATE)
            .tmdbId(DEFAULT_TMDB_ID)
            .userId(DEFAULT_USER_ID);
        return myMovie;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MyMovie createUpdatedEntity(EntityManager em) {
        MyMovie myMovie = new MyMovie()
            .comments(UPDATED_COMMENTS)
            .vote(UPDATED_VOTE)
            .viewedDate(UPDATED_VIEWED_DATE)
            .tmdbId(UPDATED_TMDB_ID)
            .userId(UPDATED_USER_ID);
        return myMovie;
    }

    @BeforeEach
    public void initTest() {
        myMovie = createEntity(em);
    }

    @Test
    @Transactional
    void createMyMovie() throws Exception {
        int databaseSizeBeforeCreate = myMovieRepository.findAll().size();
        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);
        restMyMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeCreate + 1);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(DEFAULT_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(DEFAULT_VIEWED_DATE);
        assertThat(testMyMovie.getTmdbId()).isEqualTo(DEFAULT_TMDB_ID);
        assertThat(testMyMovie.getUserId()).isEqualTo(DEFAULT_USER_ID);
    }

    @Test
    @Transactional
    void createMyMovieWithExistingId() throws Exception {
        // Create the MyMovie with an existing ID
        myMovie.setId(1L);
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        int databaseSizeBeforeCreate = myMovieRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMyMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTmdbIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = myMovieRepository.findAll().size();
        // set the field null
        myMovie.setTmdbId(null);

        // Create the MyMovie, which fails.
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        restMyMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = myMovieRepository.findAll().size();
        // set the field null
        myMovie.setUserId(null);

        // Create the MyMovie, which fails.
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        restMyMovieMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMyMovies() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].vote").value(hasItem(DEFAULT_VOTE)))
            .andExpect(jsonPath("$.[*].viewedDate").value(hasItem(DEFAULT_VIEWED_DATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));
    }

    @Test
    @Transactional
    void getMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get the myMovie
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL_ID, myMovie.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(myMovie.getId().intValue()))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS))
            .andExpect(jsonPath("$.vote").value(DEFAULT_VOTE))
            .andExpect(jsonPath("$.viewedDate").value(DEFAULT_VIEWED_DATE.toString()))
            .andExpect(jsonPath("$.tmdbId").value(DEFAULT_TMDB_ID))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID.intValue()));
    }

    @Test
    @Transactional
    void getMyMoviesByIdFiltering() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        Long id = myMovie.getId();

        defaultMyMovieShouldBeFound("id.equals=" + id);
        defaultMyMovieShouldNotBeFound("id.notEquals=" + id);

        defaultMyMovieShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMyMovieShouldNotBeFound("id.greaterThan=" + id);

        defaultMyMovieShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMyMovieShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments equals to DEFAULT_COMMENTS
        defaultMyMovieShouldBeFound("comments.equals=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments equals to UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.equals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments not equals to DEFAULT_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.notEquals=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments not equals to UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.notEquals=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments in DEFAULT_COMMENTS or UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.in=" + DEFAULT_COMMENTS + "," + UPDATED_COMMENTS);

        // Get all the myMovieList where comments equals to UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.in=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments is not null
        defaultMyMovieShouldBeFound("comments.specified=true");

        // Get all the myMovieList where comments is null
        defaultMyMovieShouldNotBeFound("comments.specified=false");
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments contains DEFAULT_COMMENTS
        defaultMyMovieShouldBeFound("comments.contains=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments contains UPDATED_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.contains=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void getAllMyMoviesByCommentsNotContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where comments does not contain DEFAULT_COMMENTS
        defaultMyMovieShouldNotBeFound("comments.doesNotContain=" + DEFAULT_COMMENTS);

        // Get all the myMovieList where comments does not contain UPDATED_COMMENTS
        defaultMyMovieShouldBeFound("comments.doesNotContain=" + UPDATED_COMMENTS);
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote equals to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.equals=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote equals to UPDATED_VOTE
        defaultMyMovieShouldNotBeFound("vote.equals=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote not equals to DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.notEquals=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote not equals to UPDATED_VOTE
        defaultMyMovieShouldBeFound("vote.notEquals=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote in DEFAULT_VOTE or UPDATED_VOTE
        defaultMyMovieShouldBeFound("vote.in=" + DEFAULT_VOTE + "," + UPDATED_VOTE);

        // Get all the myMovieList where vote equals to UPDATED_VOTE
        defaultMyMovieShouldNotBeFound("vote.in=" + UPDATED_VOTE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is not null
        defaultMyMovieShouldBeFound("vote.specified=true");

        // Get all the myMovieList where vote is null
        defaultMyMovieShouldNotBeFound("vote.specified=false");
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is greater than or equal to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.greaterThanOrEqual=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is greater than or equal to (DEFAULT_VOTE + 1)
        defaultMyMovieShouldNotBeFound("vote.greaterThanOrEqual=" + (DEFAULT_VOTE + 1));
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is less than or equal to DEFAULT_VOTE
        defaultMyMovieShouldBeFound("vote.lessThanOrEqual=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is less than or equal to SMALLER_VOTE
        defaultMyMovieShouldNotBeFound("vote.lessThanOrEqual=" + SMALLER_VOTE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsLessThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is less than DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.lessThan=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is less than (DEFAULT_VOTE + 1)
        defaultMyMovieShouldBeFound("vote.lessThan=" + (DEFAULT_VOTE + 1));
    }

    @Test
    @Transactional
    void getAllMyMoviesByVoteIsGreaterThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where vote is greater than DEFAULT_VOTE
        defaultMyMovieShouldNotBeFound("vote.greaterThan=" + DEFAULT_VOTE);

        // Get all the myMovieList where vote is greater than SMALLER_VOTE
        defaultMyMovieShouldBeFound("vote.greaterThan=" + SMALLER_VOTE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate equals to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.equals=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.equals=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate not equals to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.notEquals=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate not equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.notEquals=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate in DEFAULT_VIEWED_DATE or UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.in=" + DEFAULT_VIEWED_DATE + "," + UPDATED_VIEWED_DATE);

        // Get all the myMovieList where viewedDate equals to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.in=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is not null
        defaultMyMovieShouldBeFound("viewedDate.specified=true");

        // Get all the myMovieList where viewedDate is null
        defaultMyMovieShouldNotBeFound("viewedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is greater than or equal to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.greaterThanOrEqual=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is greater than or equal to UPDATED_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.greaterThanOrEqual=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is less than or equal to DEFAULT_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.lessThanOrEqual=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is less than or equal to SMALLER_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.lessThanOrEqual=" + SMALLER_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsLessThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is less than DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.lessThan=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is less than UPDATED_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.lessThan=" + UPDATED_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByViewedDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where viewedDate is greater than DEFAULT_VIEWED_DATE
        defaultMyMovieShouldNotBeFound("viewedDate.greaterThan=" + DEFAULT_VIEWED_DATE);

        // Get all the myMovieList where viewedDate is greater than SMALLER_VIEWED_DATE
        defaultMyMovieShouldBeFound("viewedDate.greaterThan=" + SMALLER_VIEWED_DATE);
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId equals to DEFAULT_TMDB_ID
        defaultMyMovieShouldBeFound("tmdbId.equals=" + DEFAULT_TMDB_ID);

        // Get all the myMovieList where tmdbId equals to UPDATED_TMDB_ID
        defaultMyMovieShouldNotBeFound("tmdbId.equals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId not equals to DEFAULT_TMDB_ID
        defaultMyMovieShouldNotBeFound("tmdbId.notEquals=" + DEFAULT_TMDB_ID);

        // Get all the myMovieList where tmdbId not equals to UPDATED_TMDB_ID
        defaultMyMovieShouldBeFound("tmdbId.notEquals=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId in DEFAULT_TMDB_ID or UPDATED_TMDB_ID
        defaultMyMovieShouldBeFound("tmdbId.in=" + DEFAULT_TMDB_ID + "," + UPDATED_TMDB_ID);

        // Get all the myMovieList where tmdbId equals to UPDATED_TMDB_ID
        defaultMyMovieShouldNotBeFound("tmdbId.in=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId is not null
        defaultMyMovieShouldBeFound("tmdbId.specified=true");

        // Get all the myMovieList where tmdbId is null
        defaultMyMovieShouldNotBeFound("tmdbId.specified=false");
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId contains DEFAULT_TMDB_ID
        defaultMyMovieShouldBeFound("tmdbId.contains=" + DEFAULT_TMDB_ID);

        // Get all the myMovieList where tmdbId contains UPDATED_TMDB_ID
        defaultMyMovieShouldNotBeFound("tmdbId.contains=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByTmdbIdNotContainsSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where tmdbId does not contain DEFAULT_TMDB_ID
        defaultMyMovieShouldNotBeFound("tmdbId.doesNotContain=" + DEFAULT_TMDB_ID);

        // Get all the myMovieList where tmdbId does not contain UPDATED_TMDB_ID
        defaultMyMovieShouldBeFound("tmdbId.doesNotContain=" + UPDATED_TMDB_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId equals to DEFAULT_USER_ID
        defaultMyMovieShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId equals to UPDATED_USER_ID
        defaultMyMovieShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId not equals to DEFAULT_USER_ID
        defaultMyMovieShouldNotBeFound("userId.notEquals=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId not equals to UPDATED_USER_ID
        defaultMyMovieShouldBeFound("userId.notEquals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultMyMovieShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the myMovieList where userId equals to UPDATED_USER_ID
        defaultMyMovieShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId is not null
        defaultMyMovieShouldBeFound("userId.specified=true");

        // Get all the myMovieList where userId is null
        defaultMyMovieShouldNotBeFound("userId.specified=false");
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId is greater than or equal to DEFAULT_USER_ID
        defaultMyMovieShouldBeFound("userId.greaterThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId is greater than or equal to UPDATED_USER_ID
        defaultMyMovieShouldNotBeFound("userId.greaterThanOrEqual=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId is less than or equal to DEFAULT_USER_ID
        defaultMyMovieShouldBeFound("userId.lessThanOrEqual=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId is less than or equal to SMALLER_USER_ID
        defaultMyMovieShouldNotBeFound("userId.lessThanOrEqual=" + SMALLER_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsLessThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId is less than DEFAULT_USER_ID
        defaultMyMovieShouldNotBeFound("userId.lessThan=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId is less than UPDATED_USER_ID
        defaultMyMovieShouldBeFound("userId.lessThan=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void getAllMyMoviesByUserIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        // Get all the myMovieList where userId is greater than DEFAULT_USER_ID
        defaultMyMovieShouldNotBeFound("userId.greaterThan=" + DEFAULT_USER_ID);

        // Get all the myMovieList where userId is greater than SMALLER_USER_ID
        defaultMyMovieShouldBeFound("userId.greaterThan=" + SMALLER_USER_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMyMovieShouldBeFound(String filter) throws Exception {
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(myMovie.getId().intValue())))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)))
            .andExpect(jsonPath("$.[*].vote").value(hasItem(DEFAULT_VOTE)))
            .andExpect(jsonPath("$.[*].viewedDate").value(hasItem(DEFAULT_VIEWED_DATE.toString())))
            .andExpect(jsonPath("$.[*].tmdbId").value(hasItem(DEFAULT_TMDB_ID)))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID.intValue())));

        // Check, that the count call also returns 1
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMyMovieShouldNotBeFound(String filter) throws Exception {
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMyMovieMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMyMovie() throws Exception {
        // Get the myMovie
        restMyMovieMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();

        // Update the myMovie
        MyMovie updatedMyMovie = myMovieRepository.findById(myMovie.getId()).get();
        // Disconnect from session so that the updates on updatedMyMovie are not directly saved in db
        em.detach(updatedMyMovie);
        updatedMyMovie
            .comments(UPDATED_COMMENTS)
            .vote(UPDATED_VOTE)
            .viewedDate(UPDATED_VIEWED_DATE)
            .tmdbId(UPDATED_TMDB_ID)
            .userId(UPDATED_USER_ID);
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(updatedMyMovie);

        restMyMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, myMovieDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isOk());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(UPDATED_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(UPDATED_VIEWED_DATE);
        assertThat(testMyMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMyMovie.getUserId()).isEqualTo(UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void putNonExistingMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, myMovieDTO.getId())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .with(csrf())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMyMovieWithPatch() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();

        // Update the myMovie using partial update
        MyMovie partialUpdatedMyMovie = new MyMovie();
        partialUpdatedMyMovie.setId(myMovie.getId());

        partialUpdatedMyMovie.viewedDate(UPDATED_VIEWED_DATE).tmdbId(UPDATED_TMDB_ID).userId(UPDATED_USER_ID);

        restMyMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMyMovie.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMyMovie))
            )
            .andExpect(status().isOk());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(DEFAULT_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(DEFAULT_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(UPDATED_VIEWED_DATE);
        assertThat(testMyMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMyMovie.getUserId()).isEqualTo(UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void fullUpdateMyMovieWithPatch() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();

        // Update the myMovie using partial update
        MyMovie partialUpdatedMyMovie = new MyMovie();
        partialUpdatedMyMovie.setId(myMovie.getId());

        partialUpdatedMyMovie
            .comments(UPDATED_COMMENTS)
            .vote(UPDATED_VOTE)
            .viewedDate(UPDATED_VIEWED_DATE)
            .tmdbId(UPDATED_TMDB_ID)
            .userId(UPDATED_USER_ID);

        restMyMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMyMovie.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMyMovie))
            )
            .andExpect(status().isOk());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
        MyMovie testMyMovie = myMovieList.get(myMovieList.size() - 1);
        assertThat(testMyMovie.getComments()).isEqualTo(UPDATED_COMMENTS);
        assertThat(testMyMovie.getVote()).isEqualTo(UPDATED_VOTE);
        assertThat(testMyMovie.getViewedDate()).isEqualTo(UPDATED_VIEWED_DATE);
        assertThat(testMyMovie.getTmdbId()).isEqualTo(UPDATED_TMDB_ID);
        assertThat(testMyMovie.getUserId()).isEqualTo(UPDATED_USER_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, myMovieDTO.getId())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMyMovie() throws Exception {
        int databaseSizeBeforeUpdate = myMovieRepository.findAll().size();
        myMovie.setId(count.incrementAndGet());

        // Create the MyMovie
        MyMovieDTO myMovieDTO = myMovieMapper.toDto(myMovie);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMyMovieMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .with(csrf())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(myMovieDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MyMovie in the database
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMyMovie() throws Exception {
        // Initialize the database
        myMovieRepository.saveAndFlush(myMovie);

        int databaseSizeBeforeDelete = myMovieRepository.findAll().size();

        // Delete the myMovie
        restMyMovieMockMvc
            .perform(delete(ENTITY_API_URL_ID, myMovie.getId()).with(csrf()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MyMovie> myMovieList = myMovieRepository.findAll();
        assertThat(myMovieList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
