package com.mgoulene.mmdb.cucumber;

import com.mgoulene.mmdb.MmdbmsuserApp;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = MmdbmsuserApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
